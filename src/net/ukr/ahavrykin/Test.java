package net.ukr.ahavrykin;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.METHOD;

@Target(value = METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Test {
    int a();

    int b();
}
