package net.ukr.ahavrykin;

import java.lang.reflect.Method;


public class Main {

      @Test (a=2, b=5)
      public static void test(int a, int b) {
          Main ob = new Main();
          try {
              Class<?> c = ob.getClass();

              Method m = c.getMethod("test", int.class, int.class);
              Test an = m.getAnnotation(Test.class);
              System.out.println(an.a() + " " + an.b());
          } catch (NoSuchMethodException exception) {
              System.out.println("Method is not found ");
          }
      }
        public static void main (String args []) {
            test(5, 10);
          }
    }

